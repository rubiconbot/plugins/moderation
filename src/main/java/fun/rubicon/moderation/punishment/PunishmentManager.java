package fun.rubicon.moderation.punishment;

import lombok.Getter;
import net.dv8tion.jda.core.Permission;
import net.dv8tion.jda.core.entities.*;

import java.util.*;
import java.util.stream.Collectors;

public class PunishmentManager {

    @Getter
    private final ArrayList<Punishment> punishmentsCache = new ArrayList<>();

    public List<Punishment> getMuteCache(){
        return punishmentsCache.parallelStream().filter(punishment -> punishment.getType().equals(PunishmentType.MUTE)).collect(Collectors.toList());
    }

    private List<Punishment> getBanCache(){
        return punishmentsCache.parallelStream().filter(punishment -> punishment.getType().equals(PunishmentType.BAN)).collect(Collectors.toList());
    }

    public boolean isMuted(Member member){
        return !getMuteCache().parallelStream().filter(punishment -> punishment.getUser().getId().equals(member.getUser().getId()) && punishment.getGuild().getId().equals(member.getGuild().getId())).collect(Collectors.toList()).isEmpty();
    }

    public Punishment getPunishmentByUser(User user, Guild guild, PunishmentType type){
        if(!isMuted(guild.getMember(user)) && !isBanned(user, guild))
            return null;
        return getPunishmentsCache().parallelStream().filter(punishment -> punishment.getGuild().getId().equals(guild.getId()) && punishment.getUser().getId().equals(user.getId()) && punishment.getType().equals(type)).collect(Collectors.toList()).get(0);
    }

    public boolean isBanned(User user, Guild guild){
        return !getBanCache().parallelStream().filter(punishment -> punishment.getUser().getId().equals(user.getId()) && punishment.getGuild().getId().equals(guild.getId())).collect(Collectors.toList()).isEmpty();
    }
    
    private Punishment mute(Member member){
        Punishment punishment = new Punishment(PunishmentType.MUTE, member.getUser(), member.getGuild());
        punishmentsCache.add(punishment);
        member.getGuild().getController().addSingleRoleToMember(member, getMutedRole(member.getGuild())).queue();
        return punishment;
    }
    
    public void mutePermanent(Member member){
        mute(member).save();
    }

    public void muteTemporary(Member member, Date expiry){
        Punishment punishment = mute(member);
        punishment.setExpiry(expiry.getTime());
        punishment.save();
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                punishment.unPunish();
            }
        }, expiry);
    }

    private Punishment ban(Member member){
        Punishment punishment = new Punishment(PunishmentType.BAN, member.getUser(), member.getGuild());
        punishmentsCache.add(punishment);
        member.getGuild().getController().ban(member, 0).queue();
        return punishment;
    }

    public void banPermanent(Member member) {
        ban(member).save();
    }

    public void banTemporary(Member member, Date expiry){
        Punishment punishment = ban(member);
        punishment.setExpiry(expiry.getTime());
        punishment.save();
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                punishment.unPunish();
            }
        }, expiry);
    }

    private Role getMutedRole(Guild guild){
        Role role;
        if(guild.getRolesByName("rubicon-muted", false).isEmpty())
            role = guild.getController().createRole().setName("rubicon-muted").setMentionable(false).complete();
        else
            role = guild.getRolesByName("rubicon-muted", false).get(0);
        setPermissions(role);
        return role;
    }

    private void setPermissions(Role role){
        role.getGuild().getCategories().forEach(category -> {
            PermissionOverride permissionOverride;
            if(category.getPermissionOverride(role) == null) {
                permissionOverride = category.createPermissionOverride(role).complete();
                permissionOverride.getManager()
                        .deny(Permission.MESSAGE_WRITE).queue();
                permissionOverride.getManager()
                        .deny(Permission.MESSAGE_ADD_REACTION).queue();
            }
        });
    }



}
