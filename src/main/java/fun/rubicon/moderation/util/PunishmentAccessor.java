package fun.rubicon.moderation.util;

import com.datastax.driver.mapping.Result;
import com.datastax.driver.mapping.annotations.Accessor;
import com.datastax.driver.mapping.annotations.Query;
import fun.rubicon.moderation.punishment.Punishment;

@Accessor
public interface PunishmentAccessor {

    @Query("SELECT * FROM punishments")
    Result<Punishment> getAll();
}
