package fun.rubicon.moderation.commands;

import fun.rubicon.plugin.command.Command;
import fun.rubicon.plugin.command.CommandCategory;
import fun.rubicon.plugin.command.CommandEvent;
import fun.rubicon.plugin.command.Result;
import net.dv8tion.jda.core.Permission;
import net.dv8tion.jda.core.entities.Member;

public abstract class PermissionHelper extends Command {

    public PermissionHelper(String[] invocations, CommandCategory commandCategory, String usage, String description) {
        super(invocations, commandCategory, usage, description);
    }

    Result checkPermissions(CommandEvent commandEvent, Member victim){
        if (victim.equals(commandEvent.getGuild().getSelfMember()))
            return new Result(error("You cannot interact Rubicon.","It is not possible to Punish Rubicon!"));
        if(!commandEvent.getMember().canInteract(victim))
            return new Result(error("No permissions", "You have no permission to interact with " + victim.getAsMention()));
        if(!commandEvent.getGuild().getSelfMember().canInteract(victim))
            return new Result(error("No permissions", String.format("Rubicon has no permissions to interact with %s", victim.getAsMention())));
        if(!commandEvent.getGuild().getSelfMember().hasPermission(Permission.MANAGE_ROLES, Permission.MANAGE_CHANNEL))
            return new Result(error("No permissions", "Rubicon needs to have `MANAGE_ROLES` and `MANAGE_CHANNEL`"));
        return null;
    }


}
