package fun.rubicon.moderation.commands;

import fun.rubicon.plugin.command.Command;
import fun.rubicon.plugin.command.CommandCategory;
import fun.rubicon.plugin.command.CommandEvent;
import fun.rubicon.plugin.command.Result;

public class UnBanCommand extends Command {

    public UnBanCommand() {
        super(new String[] {"unban"},CommandCategory.MODERATION, "", "Unban members who were banned with rubicon");
    }

    @Override
    public Result execute(CommandEvent commandEvent, String[] strings) {
        return send(info("How to unban users!","To unban members you must open the Discord server administration -> Bans -> Unban. Rubicon will automatically remove tempbans"));
    }
}
