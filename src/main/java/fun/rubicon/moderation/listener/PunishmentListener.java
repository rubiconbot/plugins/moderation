package fun.rubicon.moderation.listener;

import fun.rubicon.moderation.ModerationPlugin;
import fun.rubicon.moderation.punishment.Punishment;
import fun.rubicon.moderation.punishment.PunishmentManager;
import fun.rubicon.moderation.punishment.PunishmentType;
import net.dv8tion.jda.core.events.guild.GuildUnbanEvent;
import net.dv8tion.jda.core.events.guild.member.GuildMemberJoinEvent;
import net.dv8tion.jda.core.events.role.RoleDeleteEvent;
import net.dv8tion.jda.core.hooks.ListenerAdapter;

public class PunishmentListener extends ListenerAdapter {

    private PunishmentManager punishmentManager = ModerationPlugin.getInstance().getPunishmentManager();

    @Override
    public void onRoleDelete(RoleDeleteEvent event) {
        if(event.getRole().getName().equals("rubicon-muted"))
            ModerationPlugin.getInstance().getPunishmentManager().getMuteCache().forEach(Punishment::unPunish);
    }

    @Override
    public void onGuildMemberJoin(GuildMemberJoinEvent event) {
        if(punishmentManager.isMuted(event.getMember()))
            event.getGuild().getController().addSingleRoleToMember(event.getMember(), event.getGuild().getRolesByName("rubicon-muted", false).get(0)).queue();
    }

    @Override
    public void onGuildUnban(GuildUnbanEvent event) {
        if(punishmentManager.isBanned(event.getUser(), event.getGuild()))
            punishmentManager.getPunishmentByUser(event.getUser(), event.getGuild(), PunishmentType.BAN).unPunish();
    }
}
