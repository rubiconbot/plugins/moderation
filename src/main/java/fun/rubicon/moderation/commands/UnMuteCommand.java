package fun.rubicon.moderation.commands;

import fun.rubicon.moderation.ModerationPlugin;
import fun.rubicon.moderation.punishment.PunishmentManager;
import fun.rubicon.moderation.punishment.PunishmentType;
import fun.rubicon.plugin.command.CommandCategory;
import fun.rubicon.plugin.command.CommandEvent;
import fun.rubicon.plugin.command.Result;
import net.dv8tion.jda.core.entities.Member;
import net.dv8tion.jda.core.entities.Message;



public class UnMuteCommand extends PermissionHelper {

    private PunishmentManager punishmentManager = ModerationPlugin.getInstance().getPunishmentManager();

    public UnMuteCommand() {
        super(new String[] {"unmute"},  CommandCategory.MODERATION, "<@User> [time]", "Unmute users, who were muted by Rubicon");
    }

    @Override
    public Result execute(CommandEvent commandEvent, String[] args) {
        Message message = commandEvent.getMessage();
        if(args.length == 0)
            return sendHelp(commandEvent);
        if(message.getMentionedMembers().isEmpty())
            return send(error("Unknown user", "You have to mention an user"));
        Member victim = message.getMentionedMembers().get(0);
        Result result = checkPermissions(commandEvent, victim);
        if(result != null)
            return result;
        punishmentManager.getPunishmentByUser(victim.getUser(), victim.getGuild(), PunishmentType.MUTE).unPunish();
        return send(success("Successfully unmuted", String.format("Successfully unmuted %s", victim.getAsMention())));
    }
}
