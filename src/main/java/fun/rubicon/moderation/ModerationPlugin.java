package fun.rubicon.moderation;


import fun.rubicon.moderation.commands.BanCommand;
import fun.rubicon.moderation.commands.MuteCommand;
import fun.rubicon.moderation.commands.UnBanCommand;
import fun.rubicon.moderation.commands.UnMuteCommand;
import fun.rubicon.moderation.listener.PunishmentListener;
import fun.rubicon.moderation.punishment.PunishmentManager;
import fun.rubicon.moderation.util.PunishmentAccessor;
import fun.rubicon.plugin.Plugin;
import fun.rubicon.plugin.io.db.Cassandra;
import lombok.Getter;
import lombok.extern.log4j.Log4j;

import java.text.SimpleDateFormat;

@Log4j
public class ModerationPlugin extends Plugin {

    @Getter
    private static ModerationPlugin instance;
    @Getter
    private PunishmentManager punishmentManager;
    @Getter
    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm MM.dd.yyyy");

    @Override
    public void init() {
        instance = this;
        punishmentManager = new PunishmentManager();
        registerCommand(new BanCommand());
        registerCommand(new UnBanCommand());
        registerCommand(new MuteCommand());
        registerCommand(new UnMuteCommand());
        registerListener(new PunishmentListener());
    }

    @Override
    public void onEnable() {
        createTable();
        initCache();
    }

    private void initCache() {
        PunishmentAccessor accessor = Cassandra.getCassandra().getMappingManager().createAccessor(PunishmentAccessor.class);
        accessor.getAll().all().forEach(punishment -> {
            punishmentManager.getPunishmentsCache().add(punishment.init());
            log.debug(String.format("[PunishmentCache] Cached punishment %s", punishment.string()));
        });

    }

    private void createTable(){
        Cassandra.getCassandra().getConnection().executeAsync(
                "CREATE TABLE IF NOT EXISTS punishments (" +
                        "user_id bigint," +
                        "guild_id bigint," +
                        "expiry bigint," +
                        "type text," +
                        "primary key (user_id, guild_id) " +
                        ");");
    }
}
