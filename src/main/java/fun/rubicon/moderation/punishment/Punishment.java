package fun.rubicon.moderation.punishment;

import com.datastax.driver.mapping.annotations.Column;
import com.datastax.driver.mapping.annotations.PartitionKey;
import com.datastax.driver.mapping.annotations.Table;
import com.datastax.driver.mapping.annotations.Transient;
import fun.rubicon.moderation.ModerationPlugin;
import fun.rubicon.plugin.io.db.Cassandra;
import fun.rubicon.plugin.io.db.DatabaseEntity;

import lombok.AccessLevel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Setter;
import lombok.extern.log4j.Log4j;
import net.dv8tion.jda.core.Permission;
import net.dv8tion.jda.core.entities.Guild;
import net.dv8tion.jda.core.entities.User;

import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;
import java.util.function.Consumer;
import java.util.stream.Collectors;

@EqualsAndHashCode(callSuper = true)
@Data
@Table(name = "punishments")
@Log4j
public class Punishment extends DatabaseEntity<Punishment> {

    @Transient
    @Setter(AccessLevel.NONE)
    private PunishmentType type;
    @Column(name = "type")
    @Setter(AccessLevel.NONE)
    private String typeString;
    @Column(name = "user_id")
    @PartitionKey
    @Setter(AccessLevel.NONE)
    private long userId;
    @Transient
    @Setter(AccessLevel.NONE)
    private User user;
    @Column(name = "guild_id")
    @PartitionKey(1)
    @Setter(AccessLevel.NONE)
    private long guildId;
    @Transient
    @Setter(AccessLevel.NONE)
    private Guild guild;
    @Setter
    private long expiry;

    Punishment(PunishmentType type, User user, Guild guild) {
        super(Punishment.class, Cassandra.getCassandra());
        this.type = type;
        this.typeString = type.toString();
        this.user = user;
        this.userId = user.getIdLong();
        this.guild = guild;
        this.guildId = guild.getIdLong();
        this.expiry = 0L;
    }

    /*Constructor for Cassandra*/
    public Punishment() {
        super(Punishment.class, Cassandra.getCassandra());
    }

    public Punishment init() {
        this.type = PunishmentType.valueOf(typeString);
        this.guild = ModerationPlugin.getInstance().getRubicon().getShardManager().getGuildById(guildId);
        this.user = ModerationPlugin.getInstance().getRubicon().getShardManager().getUserById(userId);
        if(expiry != 0L)
            new Timer().schedule(new TimerTask() {
                @Override
                public void run() {
                    unPunish();
                }
            }, new Date(expiry));
        return this;
    }



    public void unPunish(){
        ModerationPlugin.getInstance().getPunishmentManager().getPunishmentsCache().remove(this);

        switch (type){
            case MUTE:
                if(guild.getMember(user).getRoles().stream().filter(role -> role.getName().equals("rubicon-muted")).collect(Collectors.toList()).isEmpty())
                    return;
                if(!guild.getSelfMember().canInteract(guild.getMember(user))){
                    guild.getOwner().getUser().openPrivateChannel().complete().sendMessage(String.format("Could not unpunish %s#%s Reason: UNINTERACTABLE", user.getName(), user.getDiscriminator())).complete();
                    return;
                }
                if(!guild.getSelfMember().hasPermission(Permission.MANAGE_ROLES)) {
                    guild.getOwner().getUser().openPrivateChannel().complete().sendMessage(String.format("Could not unpunish %s#%s Reason: Missing MANAGE_ROLES permission", user.getName(), user.getDiscriminator())).complete();
                    return;
                }
                guild.getController().removeSingleRoleFromMember(guild.getMember(user), guild.getRolesByName("rubicon-muted", false).get(0)).queue();
                break;
            case BAN:
                if(!guild.getSelfMember().hasPermission(Permission.BAN_MEMBERS)){
                    guild.getOwner().getUser().openPrivateChannel().complete().sendMessage(String.format("Could not unpunish %s#%s Reason: Missing BAN_MEMBERS permission", user.getName(), user.getDiscriminator())).complete();
                    return;
                }
                guild.getController().unban(String.valueOf(userId)).queue();
                break;
            default:
                log.error("[Punishment] Error unknown punishment type");
                break;
        }
        log.debug("[DB] Deleting punishment " + string());
        delete();
    }

    public String string(){
        return String.format("%s: [User: %s, Guild: %s,  Expiry: %s]", type.getDisplayName(), userId, guildId, expiry);
    }

    @Override
    public void save() {
        save(this, null, null);
    }

    @Override
    public void save(Consumer<Punishment> onSuccess) {
        save(this, onSuccess, null);
    }

    @Override
    public void save(Consumer<Punishment> onSuccess, Consumer<Throwable> onError) {
        save(this, onSuccess, onError);
    }

    @Override
    public void delete() {
        delete(this, null, null);
    }

    @Override
    public void delete(Consumer<Punishment> onSuccess) {
        save(this, onSuccess, null);
    }

    @Override
    public void delete(Consumer<Punishment> onSuccess, Consumer<Throwable> onError) {
        save(this, onSuccess, onError);
    }

}
