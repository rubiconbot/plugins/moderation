package fun.rubicon.moderation.punishment;

import lombok.Getter;

public enum PunishmentType {

    MUTE("Mute"),
    BAN("Ban");

    @Getter
    private String displayName;

    PunishmentType(String displayName){
        this.displayName = displayName;
    }
}
