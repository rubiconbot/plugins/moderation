package fun.rubicon.moderation.util;

import java.util.Calendar;
import java.util.Date;

public class DateUtil {

    public static Date parseString(String identifier){
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        int amount = parseInt(identifier);
        if(amount == 0) return  null;
        if (identifier.contains("d"))
            cal.add(Calendar.DAY_OF_MONTH, amount);
        else if (identifier.contains("m"))
            cal.add(Calendar.MINUTE, amount);
        else if (identifier.contains("y"))
            cal.add(Calendar.YEAR, amount);
        else if (identifier.contains("M"))
            cal.add(Calendar.MONTH, amount);
        else if (identifier.contains("h"))
            cal.add(Calendar.HOUR_OF_DAY, amount);
        else if(identifier.contains("s"))
            cal.add(Calendar.SECOND, amount);
        else
            return null;
        return cal.getTime();
    }

    private static Integer parseInt(String integer){
        try {
            return Integer.parseInt(integer.replace("d", "").replace("m", "").replace("y", "").replace("M", "").replace("h", "").replace("s", ""));
        } catch (NumberFormatException e){
            return 0;
        }
    }
}
