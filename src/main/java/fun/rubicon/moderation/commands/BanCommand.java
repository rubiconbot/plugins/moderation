package fun.rubicon.moderation.commands;

import fun.rubicon.moderation.ModerationPlugin;
import fun.rubicon.moderation.punishment.PunishmentManager;
import fun.rubicon.moderation.util.DateUtil;
import fun.rubicon.plugin.command.CommandCategory;
import fun.rubicon.plugin.command.CommandEvent;
import fun.rubicon.plugin.command.Result;
import net.dv8tion.jda.core.entities.Member;
import net.dv8tion.jda.core.entities.Message;

import java.util.Date;

public class BanCommand extends PermissionHelper {

    private PunishmentManager punishmentManager = ModerationPlugin.getInstance().getPunishmentManager();

    public BanCommand() {
        super(new String[] {"ban", "tempban"}, CommandCategory.MODERATION, "<@User> [time]", "Ban members temporary or permanent");
    }

    @Override
    public Result execute(CommandEvent commandEvent, String[] args) {
        Message message = commandEvent.getMessage();
        if(args.length == 0)
            return sendHelp(commandEvent);
        if(message.getMentionedMembers().isEmpty())
            return send(error("Unknown user", "You have to mention an user"));
        Member victim = message.getMentionedMembers().get(0);
        String rawArgs = commandEvent.getArgsAsString().replace("@", "x").replace(victim.getEffectiveName(), "");
        String[] newArgs = rawArgs.split(" ");
        if(punishmentManager.isBanned(victim.getUser(), victim.getGuild()))
            return send(error("Already banned", "This user is already banned"));
        Result result = checkPermissions(commandEvent, victim);
        if(result != null)
            return result;
        if(newArgs.length == 1){
            //TODO: Permission for permanent ban
            punishmentManager.banPermanent(victim);
            return send(success("Successfully banned", String.format("Successfully banned user %s permanently", victim.getAsMention())));
        } else if (newArgs.length > 1) {
            Date expiry = DateUtil.parseString(newArgs[1]);
            if(expiry == null)
                return send(error("Unknown date", "Please specify a valid date"));
            punishmentManager.banTemporary(victim, expiry);
            return send(success("Successfully banned", String.format("Successfully banned %s till %s", victim.getAsMention(), ModerationPlugin.getDateFormat().format(expiry))));
        }
        return sendHelp(commandEvent);
    }
}
